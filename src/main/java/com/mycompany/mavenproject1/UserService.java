/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>(); //สร้าง array เก็บข้อมูล

    //Mockup: จำลองข้อมูล
    static {        //static บล็อคที่จะโหลดเมื่อ class โหลด
        userList.add(new User("admin", "password"));//ถ้าลบuser admin user adminจะหายไป
        userList.add(new User("user1", "password"));//ถ้าลบuser user1  user1จะไม่หายไป
    }

    //Create user
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    //Overload
    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    //Update User
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    //Read 1 user
    public static User getUser(int index) {
        if(index>userList.size()-1){
            return null;
        }
        return userList.get(index);
    }

    //Read All user 
    public static ArrayList<User> getUsers() {
        return userList;
    }

    //Search username
    public static ArrayList<User> serchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }

    //Delete user จาก index
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    //Delete user จาก object
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    //login
    public static User login(String userName, String password) {
        for(User user: userList){
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)){
                return user;
            }
        } 
        return null;
    }
    
    public static void save(){
        
    }
    public static void load(){
        
    }

}
